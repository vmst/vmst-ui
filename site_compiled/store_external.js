//@ts-check
const os = require('os')
/**
 * @typedef type_store
 * @property {string} path
 * @property {string} content_type
 * @property {string} data_base64
 */
/**
 * @typedef type_store_get
 * @property {string} path
 * @property {string} content_type
 * @property {string} data
 */
/** @type {type_store[]} */
let list = []
exports.list = list
exports.get = get
/**
 * @param {string} path
 * @returns {type_store_get}
 */
function get(path) {
    let fnd = list.find(f => typeof path === 'string' && typeof f.path === 'string' && path.toLowerCase() === f.path.toLowerCase())
    if (typeof fnd === 'object' && typeof fnd.data_base64 === 'string') {
        return {
            path: fnd.path,
            content_type: fnd.content_type,
            data: Buffer.from(fnd.data_base64, 'base64').toString('utf8')
        }
    }
    return undefined
}

list.push({path: '/vue/root.vue', content_type: 'text/html; charset=UTF-8', data_base64: 'VnVlLmNvbXBvbmVudCgicm9vdCIsIHsNCnRlbXBsYXRlOiAnPHRlbXBsYXRlPiAgICAgPGJ1dHRvbiB2LW9uOmNsaWNrPSJjb3VudCsrIj7QodGH0ZHRgtGH0LjQuiDQutC70LjQutC+0LIgMiDigJQge3sgY291bnQgfX08L2J1dHRvbj4gPC90ZW1wbGF0ZT4nLA0KZGF0YSA6ZnVuY3Rpb24gKCkgewogICAgcmV0dXJuIHsgY291bnQ6IDAgfTsKfQ0KfSk='})