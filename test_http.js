//@ts-check

const lib_conv = require('viva-convert')
const lib_http = require('viva-server-http')
const store_internal = require('./site_compiled/store_internal.js')
const store_external = require('./site_compiled/store_external.js')

let http = new lib_http()

http.on('listening', function(host) {
    console.log ('http server ' + host + ' started...')
})

http.on('post', function(data, sender, incomingMessageHttp, serverResponseHttp) {
    sender(200, data)
})

http.on('get', function(data, sender, incomingMessageHttp, serverResponseHttp) {
    let html_external = store_external.get(incomingMessageHttp.url)
    if (!lib_conv.isAbsent(html_external)) {
        serverResponseHttp.setHeader('Content-Type', html_external.content_type)
        sender(200, html_external.data)
        return
    }

    let html_internal = store_internal.get(incomingMessageHttp.url)
    if (!lib_conv.isAbsent(html_internal)) {
        serverResponseHttp.setHeader('Content-Type', html_internal.content_type)
        sender(200, html_internal.data)
        return
    }

    sender(404, lib_conv.format("unknown path {0}", incomingMessageHttp.url))
    return
})

http.on('error', function(error, sender, incomingMessageHttp, serverResponseHttp) {
    sender(500,error.toString())
})

try {
    http.start('127.0.0.1',3042)
} catch (error) {
    console.log(error)
}