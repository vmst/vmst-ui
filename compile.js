const lib_path = require('path')
const lib_viva_ui = require('../viva-ui/index.js')

lib_viva_ui.compile(lib_path.join(__dirname, 'site_source'), lib_path.join(__dirname, 'site_compiled'), 'vue/root.vue', 'root')